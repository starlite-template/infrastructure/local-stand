# Local stage infrastructure
#### Репозиторий, содержащий инфрастрруктуру для локальной разработки
# Описание

## Межкомпонентное сетевое взаимодействие
1. Инфраструктурные компоненты (redis, postgres, keycloak) общаются между собой через docker network, обращаясь к контейнерам через host_name
2. Системные компоненты отправляют исходящий трафик в Traefik через network aliases. Traefik в свою очередь отправляет запросы внутрь нужных контейнеров ориентируясь на labels


## Маршрутизация и роутинг.
В данном инфраструктурном шаблоне трафик внутрь контейнеров направляет Traefik прокси

При добавлении новых контейнеров при необходимости входного трафика следует указывать в Traefik labels метки, отвечающие за маршрутизацию и роутинг.

Для добавления входящего трафика внутрь вашего контейнера, в докер-композ файле необходимо указать следующиее лейблы:

HTTP:

    labels:
        traefik.enable: true
        traefik.http.routers.[service_name].entrypoints: [traefik_entrypoint:-web]
        traefik.http.routers.[service_name].rule: Host(`[service_name].docker.localhost`)
        # OR FOR API SERVICES
        # traefik.http.routers.[service_name].rule: "Host(`api.docker.localhost`) &&  PathPrefix(`/[service_prefix]`)"
        traefik.http.routers.[service_name].service: [service_name]
        traefik.http.services.[service_name].loadbalancer.server.port: [listining_port_in_container]
TCP\UDP

    labels:
        traefik.enable: true
        traefik.tcp.routers.[service_name].tls: false
        traefik.tcp.routers.[service_name].entrypoints: [service_name] # Не забудьте создать ентрипоинт в лейблах Траефика
        traefik.tcp.routers.[service_name].rule: HostSNI(`*`)
        traefik.tcp.routers.[service_name].service: [service_name]
        traefik.tcp.services.[service_name].loadBalancer.server.port: [listining_port_in_container]

## Heath-check проверка
Для реализации health-check проверки в инфраструктурном шаблоне с использованием, вы можете использовать параметры healthcheck в docker.

Пример настройки health-check liveness проверки:

    healthcheck:
        test: [ "CMD", "redis-cli", "--raw", "incr", "ping" ] - комманда для запуски ХС
        interval: 5s
        timeout: 5s
        retries: 5
        start_period: 5s

Пример настройки сетевого health-check Traefik:

    labels:
        traefik.http.services.[service_name].healthcheck.path: "/health"  # Пример пути для health-check
        traefik.http.services.[service_name].healthcheck.interval: "10s"   # Интервал между проверками
        traefik.http.services.[service_name].healthcheck.timeout: "3s"     # Тайм-аут проверки
        traefik.http.services.[service_name].healthcheck.retries: 3       # Количество попыток до считывания как недоступный




# Команды 

### Start
```zsh
make run-infra
```
### Stop
```zsh
make stop-infra
```
### Start OpenSearch
```zsh
make run-os
```
### Init Vault
```zsh
make vault-init
```
### Init Postgres
```zsh
make postgres-init
```


# Настройки

## необходимо прописать следующие правила в hosts на локальной машине
```
# ROLF
0.0.0.0 jaeger.docker.localhost
0.0.0.0 redis.docker.localhost
0.0.0.0 postgres.docker.localhost
0.0.0.0 auth.docker.localhost
0.0.0.0 vault.docker.localhost
0.0.0.0 kafka.docker.localhost
0.0.0.0 opensearch.docker.localhost
# API
0.0.0.0 api.docker.localhost
```
#### Для Windows 0.0.0.0 замените на 127.0.0.1

## Vault. Хранение секретов
Для подключения к Vault используйте следующие креды:
```
VAULT_HOST=vault.docker.localhost
VAULT_TOKEN=vault-root-token
```
Вольт заполняется скриптом vault/vault-init.sh в контейнере vault-init
#### Для добавления нового неймспейса необходимо вручную занести его в папку vault/namespaces!

### Заполнение секретов 
    Рекомендуется заполнять секреты в json формате учитывая вложенность. Пример:
```json
{
	"postgres": {
		"master": {
			"host": "postgres.docker.localhost",
			"port": 5432,
			"db": "postgres",
			"user": "postgres",
			"password": "postgres"
		},
		"slave": {
			"host": "postgres.docker.localhost",
			"port": 5432,
			"db": "example_db_for_service",
			"user": "postgres",
			"password": "postgres"
		}
	},
	"redis": {
		"host": "redis.docker.localhost",
		"port": 6379,
		"db": 1,
		"password": ""
	},
	"log": {
		"level": "DEBUG",
		"format": "plain"
	}
}
```
    При считывании json в config модели, следует привести все в плоскую структуру,
    добавив родительские ключи как префиксы переменным
        "postgres": {
            "master": {
                "host": "postgres.docker.localhost",
        в
        "postgres_master_host": "postgres.docker.localhost",

## OpenSearch
Для работы опенсерч необходимо увеличить параметр vm.max_map_count на хосте минимум до 262144
```zsh
sysctl -w vm.max_map_count=262144
```

## KeyCloak
При запуске кейклоака происходит импорт REALMs из папки [./keycloak](keycloak)
### Тестовые учетные записи:
#### Client:
    client_id: backend
    client_secret: OOBvk3YrnK1mNrGCmRG6bX4ESFawEZEe
#### User:
    username: test_user
    password: 12345678

Для экспорта настроек необходимо выполнить команду
```zsh
docker exec -t local-stage-keycloak-1 bash -c "/opt/keycloak/bin/kc.sh export --dir /opt/keycloak/data/export --realm rolf --users realm_file"
docker cp local-stage-keycloak-1:/opt/keycloak/data/export/local-realm.json ./keycloak/local-realm.json
```