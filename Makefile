stop-infra:
	docker compose stop && \
	docker compose rm -f -a

run-infra: stop-infra
	docker compose up -d traefik jaeger postgres redis keycloak

stop-os:
	docker compose stop opensearch-node opensearch-dashboards && \
	docker compose rm -f

run-os: stop-os
	docker compose up -d opensearch-node opensearch-dashboards

postgres-init: stop-infra
	-docker volume rm local-stand_postgres-data
	docker compose up -d postgres
